This wiki contains mainly instructions for maintainers and developers of Fractal.

For installation instructions and frequently asked questions, see [`README.md`](https://gitlab.gnome.org/World/fractal/-/blob/main/README.md).

To track or report bugs, search in our [issues](https://gitlab.gnome.org/World/fractal/-/issues).

To follow the features in development look at our [milestones](https://gitlab.gnome.org/World/fractal/-/milestones).

For build and development instructions, see [`CONTRIBUTING.md`](https://gitlab.gnome.org/World/fractal/-/blob/main/CONTRIBUTING.md).
