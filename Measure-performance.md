The following methods are available to measure the performance of Fractal:

- [Instrument functions with tracing](Measure-performance/Instrument-functions-with-tracing): measure precisely how long each method takes and what is the link between method calls even between threads. It is however limited to functions called from Fractal.