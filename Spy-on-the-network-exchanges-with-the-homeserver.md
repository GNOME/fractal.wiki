In theory, any proxy that allows to watch the traffic should work. Here we will
use [mitmproxy](https://mitmproxy.org/).

We assume that you are using one of the flatpaks of Fractal, installed from
Flathub, Flathub Beta or GNOME Nightly.

1. Install mitmproxy. It should come with three executables: mitmproxy, mitmweb,
    mitmdump.
2. Launch the executable that you prefer to use, no parameters are needed. The
    easiest is probably to use mitmweb, it will launch a web page where you can
    watch the traffic.
3. We need to make the certificate used by mitmproxy available inside the
    flatpak sandbox. In theory we should be able to do that by adding the
    certificate to the system, but it does not work because of #695+, so we will
    need to do it manually.

    Locate the certificate at `~/.mitmproxy/mitmproxy-ca-cert.pem`. It needs to
    be copied to a location that is also available inside the sandbox, so we
    will use the `XDG_CACHE_HOME` directory of the flatpak, which is located at
    `~/.var/app/{APP_ID}/cache`. The `APP_ID` is the identifier used when
    launching Fractal via the command line, so `org.gnome.Fractal` for the
    stable and beta version, and `org.gnome.Fractal.Devel` for the nightly
    version.
    
    For example, for the nightly version:
    
    ```sh
    cp ~/.mitmproxy/mitmproxy-ca-cert.pem ~/.var/app/org.gnome.Fractal.Devel/cache/
    ```
4. Now we can launch Fractal and make it use the proxy thanks to environment
    variables.

    The `ALL_PROXY` environment variable is used to provide the location where
    the proxy is listening. For mitmproxy the default should be
    `http://localhost:8080`.
    
    The `SSL_CERT_FILE` is used to provide the location of the certificate of
    the proxy. Conveniently, the path inside the Flatpak is the same, so
    `~/.var/app/{APP_ID}/cache/mitmproxy-ca-cert.pem`.

    For example, for the nightly version:
    
    ```sh
    flatpak run --env="ALL_PROXY=http://localhost:8080" --env="SSL_CERT_FILE=$HOME/.var/app/org.gnome.Fractal.Devel/cache/mitmproxy-ca-cert.pem" org.gnome.Fractal.Devel
    ```

And _voilà_! You should be able to watch the traffic in the UI of mitmproxy and
see details about each request.