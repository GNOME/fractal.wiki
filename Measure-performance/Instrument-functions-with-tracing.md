An easy way to measure the performance of functions as well as see the chain of calls between threads, is to use the [`tracing::instrument`](https://docs.rs/tracing/latest/tracing/attr.instrument.html) attribute macro.

Note that while this method gives precise spans, it is limited to measuring the functions that we call directly from Fractal. It means that the performance of most of the GTK drawing methods cannot be measured this way.

## Usage

It's use is really easy, just apply the attribute to a function, it will generate a span for that function. If you apply this attribute to a method called inside that function, both spans will be linked, even if it moves to a separate thread.

Arbitrary fields can be added to each span, allowing you to get more details when visualizing a given span later on. For all the syntax, see the docs of the macro.

Note that instrumenting functions will already affect the logs. If logging happens in a function that is instrumented, the details of the span will be prepended to the log message.

## Set up tracing-opentelemetry

Now that functions are instrumented, we need to be able to visualize them. Our choice here is to use the [tracing-opentelemetry](https://crates.io/crates/tracing-opentelemetry) crate to forward tracing data by using the [opentelemetry](https://opentelemetry.io/) protocol.

First, add the following crates to `Cargo.toml`:

```toml
opentelemetry = "0.27"
opentelemetry-otlp = "0.27"
opentelemetry_sdk = { version = "0.27", features = ["rt-tokio"] }
tracing-opentelemetry = "0.28"
```

The replace the beginning of the `main` function in `main.rs`, that initializes tracing-subscriber, with:

```rust
    spawn_tokio!(async move {
        use opentelemetry::trace::TracerProvider as _;
        let otlp_tracer = opentelemetry_sdk::trace::TracerProvider::builder()
            .with_resource(opentelemetry_sdk::Resource::new(vec![
                opentelemetry::KeyValue::new("service.name", "fractal"),
            ]))
            .with_batch_exporter(
                opentelemetry_otlp::SpanExporter::builder()
                    .with_tonic()
                    .build()
                    .unwrap(),
                opentelemetry_sdk::runtime::Tokio,
            )
            .build()
            .tracer("fractal-1");
        let opentelemetry = tracing_opentelemetry::layer().with_tracer(otlp_tracer);

        let env_filter = EnvFilter::try_from_default_env()
            .unwrap_or_else(|_| EnvFilter::new("fractal=info,warn"));

        tracing_subscriber::registry()
            .with(fmt::layer().with_filter(env_filter))
            .with(opentelemetry)
            .init();
    });
```

This sets up tracing-opentelemetry to forward the tracing data via gRPC with a service called `fractal`, and a tracer called `fractal-1`.

## Visualize spans with Jaeger

Now that Fractal is ready, we will use [Jaeger](https://www.jaegertracing.io/) to receive the data and visualize the span.

First, start it by using a container:

```sh
podman run --rm --name jaeger -p 16686:16686 -p 4317:4317 jaegertracing/jaeger:latest
```

Then start Fractal, and do what you want to do to trigger the functions that were instrumented.

The spans can be visualized in Jaeger's UI that can be accessed at http://localhost:16686. Just select `fractal` in `Service`, then optionally select a function in `Operation` and click on `Find traces`. Note that if you don't filter by function, you will also see functions instrumented by other crates.

By clicking on a trace, you can see all its spans with various representations, like a timeline, a table or a flamegraph. You can switch between them with the dropdown at the top right that should be labelled `Trace Timeline` by default. When you click on a span you can see all its details like the tags (this is where the `fields` from `tracing::instrument` appear), or even the logs that were emitted in that span.

When you are done, you can stop Jaeger by pressing <kbd>Ctrl</kbd> + <kbd>C</kbd> in the terminal.

## Visualize spans with Grafana

While the Jaeger UI can be enough to study the spans, it has limitations, notably performance issues when there are a lot of spans in a trace. A more powerful solution is to use [Grafana](https://grafana.com/oss/grafana/).

It has a tool to collect opentelemetry traces which is [Grafana Tempo](https://grafana.com/oss/tempo/), however it requires a slightly complicated setup, so we will keep using Jaeger to collect the traces, and connect Grafana to it.

First, we need to create a pod for both containers to be able to reach each other:

```sh
podman pod create -p 16686:16686 -p 4317:4317 -p 3000:3000 fractal-tracing
```

Then we can launch Jaeger like before, with its UI still available at the same address:

```sh
podman run --rm --name jaeger --pod fractal-tracing jaegertracing/jaeger:latest
```

And finally launch Grafana in another terminal tab:

```sh
podman run --rm --name grafana --pod fractal-tracing --env GF_AUTH_ANONYMOUS_ENABLED=true --env GF_AUTH_ANONYMOUS_ORG_ROLE=Admin --env GF_AUTH_DISABLE_LOGIN_FORM=true grafana/grafana-oss
```

The UI is available at http://localhost:3000. The environment variables are for disabling the login form, since we don't care about it.

Then we need to set up the Jaeger data source. First, start Fractal since Grafana doesn't allow to add a data source without any services running. Then:

1. In the main menu, select `Connections > Data sources`.
2. Click on `Add data source`.
3. Search for `jaeger` and select it.
4. In `Connection > URL`, enter `http://jaeger:16686`.
5. Click on `Save & Test` at the very bottom.
6. A success message should show up with a link to `Explore view`, click it. Alternatively, select `Explore` in the main menu.
7. There should be an initial query named `A (jaeger)`. For `Query type` click on `Search`, then you can select `fractal` for the `Service Name` and optionally an instrumented function for the `Operation Name`. Finally click on `Run query` at the top right to get the traces.

By clicking on a trace, the view should split with the timeline of the trace showing at the right. You can study the timeline as you wish, and you can even filter the spans that appear with `Span filters`.

When you are done, you can stop both programs by pressing <kbd>Ctrl</kbd> + <kbd>C</kbd> in their terminal tab. To do a thorough cleanup, you will also need to remove the pod with:

```sh
podman pod rm fractal-tracing
```