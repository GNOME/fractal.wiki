There are many ways to profile the CPU. One such example is using [Sysprof](https://developer.gnome.org/documentation/tools/sysprof.html), that can be launched easily [via GNOME Builder](https://builder.readthedocs.io/projects/profiling.html).

We are going to look here at [samply](https://github.com/mstange/samply), which uses [perf](https://perfwiki.github.io/main/) to instrument the CPU. It has the advantage of generating an output that can be loaded in the [Firefox Profiler](https://profiler.firefox.com/), which allows to have several visualizations of the performance data.

## Usage

We are going to measure the performance of Fractal Nightly here. The instructions can be modified to measure the performance of the stable version instead.

First, make sure that the same Flatpak prerequisites as in [`CONTRIBUTING.md`](https://gitlab.gnome.org/World/fractal/-/blob/main/CONTRIBUTING.md#prerequisites) are installed on the system.

Another prerequisite for samply is to set this:

```sh
sudo sysctl kernel.perf_event_paranoid=1
```

Launch a shell in the Flatpak of Fractal Nightly:

```sh
flatpak run --command="sh" --devel org.gnome.Fractal.Devel
```

Enable Rust in the shell:

```sh
source /usr/lib/sdk/rust-stable/enable.sh
```

Install samply:

```sh
cargo install --locked samply
```

Add Cargo binaries to the path, to be able to launch samply easily:

```sh
export PATH=$PATH:$HOME/.cargo/bin
```

And now we are ready to proceed to the sampling of the CPU performance data:

```sh
samply record fractal
```

When you are done, close Fractal and the Firefox Profiler should open in the browser automatically .